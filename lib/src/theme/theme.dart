import 'package:flutter/material.dart';

final miTema = ThemeData.dark().copyWith(
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    selectedItemColor: Colors.red,
  ),
  primaryColor: Colors.red,
);
